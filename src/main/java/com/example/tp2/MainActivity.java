package com.example.tp2;

import android.content.ClipData;
import android.content.Intent;
import android.database.Cursor;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listView;
    WineDbHelper data;
    SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Cursor cursor;
        //recuperer lid de la liste

        listView = (ListView) findViewById(R.id.listview);
        data = new WineDbHelper(getApplicationContext());

        if (data.fetchAllWines().getCount() < 1) { //si la base de donnée est vide on appel populate

            data.populate();
        }
//aapel a la fonction reafrichier 2eme selection pour afficher les element la base de donnée
rafrichir();


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Cursor item = (Cursor) parent.getItemAtPosition(position);
                // Création de l'intent

                Intent intent = new Intent(MainActivity.this, activity_wine.class);

                // Ajout dans l'intent de l'objet wine

                intent.putExtra("wine", data.cursorToWine(item));//transformer le curseur a un objet

                // Envoyer l'intent
                startActivity(intent);
                finish();


            }
        });

        FloatingActionButton b = findViewById(R.id.boutton);


        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //  envoyer lintent

                Intent intent1 = new Intent(MainActivity.this, activity_wine.class);

                startActivity(intent1);
                finish();
            }
        });




                registerForContextMenu(listView);//virifie que la liste est attacher a un menu






    }
//2eme selection pour afficher les element la base de donnée
    public void rafrichir(){

        final Cursor result = data.fetchAllWines();
        result.moveToFirst();


        final SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2,
                result, new String[]{WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_LOC},
                new int[]{android.R.id.text1, android.R.id.text2}, 0);


        listView.setAdapter(adapter);


    }
    //crier un menu qui vas faire delete


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.add(0, v.getId(), 0, "Delete");
    }
    //SI J SELECTIONNE DELETE IL VAS APPELER LA FONCTION DELETE DE LA BASE DE DONNÉÉ

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle() == "Delete") {
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            Cursor it = (Cursor) listView.getItemAtPosition(info.position);

            data.deleteWine(it);
            rafrichir();



        }
        return true;
    }































        }




