package com.example.tp2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import static android.database.sqlite.SQLiteDatabase.CONFLICT_IGNORE;

public class WineDbHelper extends SQLiteOpenHelper {

    private static final String TAG = WineDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "wine.db";

    public static final String TABLE_NAME = "cellar";

    public static final String _ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_WINE_REGION = "region";
    public static final String COLUMN_LOC = "localization";
    public static final String COLUMN_CLIMATE = "climate";
    public static final String COLUMN_PLANTED_AREA = "publisher";



    static final String CREATE_TABLE_LIBRARY = "CREATE TABLE "
            + TABLE_NAME + "(" + _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME
            + " TEXT, " + COLUMN_WINE_REGION+ " TEXT, " + COLUMN_LOC
            + " TEXT, " +COLUMN_CLIMATE + " TEXT, "+COLUMN_PLANTED_AREA  + " TEXT )";

    public static final String METIER_TABLE_DROP = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";

    public WineDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

	// db.execSQL() with the CREATE TABLE ... command
        db.execSQL(CREATE_TABLE_LIBRARY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(METIER_TABLE_DROP);

        // crier une base

        onCreate(db);

    }


   /**
     * Adds a new wine
     * @return  true if the wine was added to the table ; false otherwise (case when the pair (name, region) is
     * already in the data base
     */
    public boolean addWine(Wine wine) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues  Values = new ContentValues();
        Values.put(COLUMN_NAME,wine.getTitle());
        Values.put(COLUMN_WINE_REGION,wine.getRegion());
        Values.put(COLUMN_LOC, wine.getLocalization());
        Values.put(COLUMN_CLIMATE ,wine.getClimate());
        Values.put(COLUMN_PLANTED_AREA , wine.getPlantedArea());


        // Inserting Row
        long rowID = 0;
        rowID = db.insert(TABLE_NAME, null, Values);

	// call db.insert()
        db.close(); // Closing database connection

        return (rowID != -1);
    }

    /**
     * Updates the information of a wine inside the data base
     * @return the number of updated rows
     */
    public boolean updateWine(Wine wine) {

        SQLiteDatabase db = this.getWritableDatabase();
	int res=0;

        // updating row

        ContentValues updatedValues = new ContentValues();
        updatedValues.put(COLUMN_NAME,wine.getTitle());
        updatedValues.put(COLUMN_WINE_REGION,wine.getRegion());
        updatedValues.put(COLUMN_LOC, wine.getLocalization());
        updatedValues.put(COLUMN_CLIMATE ,wine.getClimate());
        updatedValues.put(COLUMN_PLANTED_AREA , wine.getPlantedArea());

        // call db.update()


        res= db.update(TABLE_NAME, updatedValues, _ID + " = ?",
                new String[] { String.valueOf(wine.getId()) });

        return true;
    }

    /**
     * Returns a cursor on all the wines of the library
     */
    public Cursor fetchAllWines() {

        // call db.query()

        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM cellar " ;


	Cursor cursor;
	// call db.query()
        cursor = db.rawQuery(selectQuery, null);
        
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;

    }

     public void deleteWine(Cursor cursor) {
        SQLiteDatabase db = this.getWritableDatabase();

         Wine wine  = cursorToWine(cursor);


         db.delete(TABLE_NAME, _ID + " = ?",
                 new String[] { String.valueOf(wine.getId()) });


        db.close();
    }

     public void populate() {
        Log.d(TAG, "call populate()");
        addWine(new Wine("Châteauneuf-du-pape", "vallée du Rhône ", "Vaucluse", "méditerranéen", "3200"));
        addWine(new Wine("Arbois", "Jura", "Jura", "continental et montagnard", "812"));
        addWine(new Wine("Beaumes-de-Venise", "vallée du Rhône", "Vaucluse", "méditerranéen", "503"));
        addWine(new Wine("Begerac", "Sud-ouest", "Dordogne", "océanique dégradé", "6934"));
        addWine(new Wine("Côte-de-Brouilly", "Beaujolais", "Rhône", "océanique et continental", "320"));
        addWine(new Wine("Muscadet", "vallée de la Loire", "Loire-Atlantique", "océanique", "9000"));
        addWine(new Wine("Bandol", "Provence", "Var", "méditerranéen", "1500"));
        addWine(new Wine("Vouvray", "Indre-et-Loire", "Indre-et-Loire", "océanique dégradé", "2000"));
        addWine(new Wine("Ayze", "Savoie", "Haute-Savoie", "continental et montagnard", "20"));
        addWine(new Wine("Meursault", "Bourgogne", "Côte-d'Or", "océanique et continental", "395"));
        addWine(new Wine("Ventoux", "Vallée du Rhône", "Vaucluse", "méditerranéen", "7450"));



        SQLiteDatabase db = this.getReadableDatabase();
        long numRows = DatabaseUtils.longForQuery(db, "SELECT COUNT(*) FROM "+TABLE_NAME, null);
        Log.d(TAG, "nb of rows="+numRows);
        db.close();
    }


    public static Wine cursorToWine(Cursor cursor) {
        Wine wine = new Wine();



        wine.setId(cursor.getLong(cursor.getColumnIndex(_ID)));

        wine.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_NAME)));

        wine.setRegion(cursor.getString(cursor.getColumnIndex(COLUMN_WINE_REGION )));
        wine.setLocalization(cursor.getString(cursor.getColumnIndex(COLUMN_LOC)));
        wine.setClimate(cursor.getString(cursor.getColumnIndex(COLUMN_CLIMATE)));

        wine.setPlantedArea(cursor.getString(cursor.getColumnIndex(COLUMN_PLANTED_AREA)));



        ;
	// build a Wine object from cursor
        return wine;
    }


    public Cursor getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("select * from " + TABLE_NAME + " where"+_ID +"=" +id, null);
        return res;
    }







}
