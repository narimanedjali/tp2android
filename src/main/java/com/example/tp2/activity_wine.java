package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class activity_wine extends AppCompatActivity {
WineDbHelper bd =new WineDbHelper(activity_wine.this);
    final   Wine wine1=new Wine();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);


        WineDbHelper vins = new WineDbHelper(this);

        final EditText Nom = (findViewById(R.id.wineName));
        final EditText region = (findViewById(R.id.editWineRegion));
        final EditText loc = (findViewById(R.id.editLoc));
        final EditText climate = (findViewById(R.id.editClimate));
        final EditText area = (findViewById(R.id.editPlantedArea));
        final Button b = (Button) findViewById(R.id.button);



        Intent intent=getIntent();

        //esk lintent contien un ellement

        if(intent.hasExtra("wine")) {

            final Wine wine = (Wine) intent.getParcelableExtra("wine");//recuperler lobjet envoyé par lintent

            Nom.setText(wine.getTitle());
            region.setText(wine.getRegion());
            loc.setText(wine.getLocalization());
            climate.setText(wine.getClimate());
            area.setText(wine.getPlantedArea());

            b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {

                    wine.setTitle(Nom.getText().toString());
                    wine.setRegion(region.getText().toString());
                    wine.setLocalization(loc.getText().toString());
                    wine.setClimate(climate.getText().toString());
                    wine.setPlantedArea(area.getText().toString());


                    if(wine.getTitle().matches("") || wine.getRegion().matches("")|| wine.getClimate().matches("")|| wine.getLocalization().matches("")|| wine.getPlantedArea().matches("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity_wine.this);
                        builder.setMessage("un champ n'est pas bien remplis");
                        builder.setCancelable(true);
                        builder.show();
                        return;

                    }


                    bd.updateWine(wine);

                    Toast.makeText(activity_wine.this, "SAUVGARDAGE DES DONNEES", Toast.LENGTH_LONG).show();


                    Intent intent2 = new Intent(activity_wine.this, MainActivity.class);
                    startActivity(intent2);
                    finish();

                }
            });

        }

        else {


            b.setOnClickListener(new View.OnClickListener() {



                @Override
                public void onClick(View v) {


                    wine1.setTitle(Nom.getText().toString());
                    wine1.setRegion(region.getText().toString());
                    wine1.setLocalization(loc.getText().toString());
                    wine1.setClimate(climate.getText().toString());
                    wine1.setPlantedArea(area.getText().toString());



                    if(wine1.getTitle().matches("") || wine1.getRegion().matches("")|| wine1.getClimate().matches("")|| wine1.getLocalization().matches("")|| wine1.getPlantedArea().matches("")){
                        AlertDialog.Builder builder = new AlertDialog.Builder(activity_wine.this);
                        builder.setMessage("un champ n'est pas bien remplis");
                        builder.setCancelable(true);
                        builder.show();
                        return;
                    }


                    else{





                        bd.addWine(wine1);
                        Intent intent2 = new Intent(activity_wine.this, MainActivity.class);
                        startActivity(intent2);




                    }



                }
            });
        }
































    }
}
